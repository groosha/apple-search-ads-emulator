def generate_items() -> list[dict]:
    result = []
    for i in range(1, 90):
        result.append({
            "id": i,
            "name": f"Item #{i}"
        })
    return result


def generate_users() -> dict:
    users = {
        "admin": {"id": 1, "first_name": "admin_first", "last_name": "admin_last"},
        "user": {"id": 2, "first_name": "user_first", "last_name": "user_last"}
    }
    return users


items = generate_items()
users = generate_users()
