from typing import Union

from fastapi import FastAPI

from server.app.schemas.outgoing import Item, User, Error
from server.app.utils import items, users

app = FastAPI()


@app.get("/items/", response_model=Item)
async def get_items(limit: int = 20, offset: int = 0):
    data = items[offset:limit + offset]
    payload = {"data": data}
    if data:
        payload["pagination"] = {
            "total_count": len(items),
            "start_num": offset,
            "in_batch": limit
        }
    return payload


@app.get("/users/{name}", response_model=Union[User, Error])
async def get_user(name: str):
    if name in users:
        return {"data": users[name]}
    return Error(reason="No such user")
