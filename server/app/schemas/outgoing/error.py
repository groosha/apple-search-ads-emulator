from pydantic import BaseModel


class Error(BaseModel):
    ok: bool = False
    reason: str = "Something bad happened"
