from typing import Optional

from pydantic import BaseModel

from .pagination import Pagination


class _SingleItem(BaseModel):
    id: int
    name: str


class Item(BaseModel):
    data: list[_SingleItem]
    pagination: Optional[Pagination]
