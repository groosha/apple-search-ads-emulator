from typing import Optional

from pydantic import BaseModel

from .pagination import Pagination


class _SingleItem(BaseModel):
    id: int
    first_name: str
    last_name: str


class User(BaseModel):
    data: _SingleItem
    pagination: Optional[Pagination]
