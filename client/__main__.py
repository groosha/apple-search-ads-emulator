import asyncio

from client.apple import Apple

api = Apple("http://127.0.0.1:8000")


async def main():
    single_user = await api.get_single_user("admin")
    print(f"[get_single_user] {single_user}")
    # ---
    items = await api.get_all_items()
    print(f"[get_all_items] {items}")


asyncio.run(main())
