from pydantic import BaseModel


class Pagination(BaseModel):
    total_count: int
    start_num: int
    in_batch: int
