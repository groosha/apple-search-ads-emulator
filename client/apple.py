import httpx
from .schemas.pagination import Pagination


class Apple:
    def __init__(self, base_url: str):
        self.base_url = base_url
        self.__client = httpx.AsyncClient(follow_redirects=True)

    async def __make_single_request(self, **kwargs):
        """
        Тупо делает один вызов API
        :return:
        """
        request = httpx.Request(**kwargs)
        result = await self.__client.send(request)
        return result.json()

    async def __fetch_data(self, method: str, path: str, **kwargs):
        """
        Забирает данные по API с пагинацией, если нужно
        :return:
        """
        url = self.make_url(path)
        list_data = []

        kwargs["method"] = method
        kwargs["url"] = url
        while True:
            result = await self.__client.request(**kwargs)
            res_json = result.json()

            if not res_json.get("pagination"):
                return res_json["data"]

            list_data.extend(res_json.get("data", []))

            pagination = Pagination.parse_obj(res_json["pagination"])
            if (pagination.start_num + pagination.in_batch) < pagination.total_count:
                kwargs.update(params={
                    "offset": pagination.start_num + pagination.in_batch,
                    "limit": 20
                    }
                )
                continue
            return list_data

    async def get_all_items(self):
        result: list = await self.__fetch_data("GET", f"/items/")
        return result

    async def get_single_user(self, name: str):
        result = await self.__fetch_data("GET", f"/users/{name}")
        return result

    def make_url(self, path: str):
        return "{}/{}".format(self.base_url.rstrip('/'), path.lstrip('/'))
